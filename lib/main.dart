import 'package:flutter/material.dart';
import 'list/list.dart';
import 'home/home.dart';
import 'favorite/favorite.dart';
import 'person/person.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
      routes: {
        '/GeneralPage': (context) => GeneralPage(),
        '/ListPage': (context) => ListPage(),
        '/FavoritePage': (context) => FavoritePage(),
        '/ProfilePage': (context) => ProfilePage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyHomePageState();
  }
}

class MyHomePageState extends State<MyHomePage> {
  var _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pandora Box"),
        backgroundColor: Colors.orange,
        leading: IconButton(
          icon: Icon(
            Icons.menu,
            semanticLabel: 'menu',
          ),
          onPressed: () {
            print('press Menu button');
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              semanticLabel: 'search',
            ),
            onPressed: () {
              print('press Search button');
            },
          ),
          IconButton(
            icon: Icon(
              Icons.tune,
              semanticLabel: 'filter',
            ),
            onPressed: () {
              print('press Filter button');
            },
          ),
        ],
      ),
      body: toPage(),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.blue,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
              backgroundColor: Colors.orange),
          BottomNavigationBarItem(
              icon: Icon(Icons.list),
              label: 'List',
              backgroundColor: Colors.red),
          BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              label: 'Favorite',
              backgroundColor: Colors.yellow),
          BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Person',
              backgroundColor: Colors.blue)
        ],
        onTap: (value) {
          setState(() {
            _currentIndex = value;
            if (value == 1) {}
          });
        },
      ),
    );
  }

  Widget toPage() {
    switch (_currentIndex) {
      case 0:
        // Navigator.pushNamed(context, "/");
        return GeneralPage();
      case 1:
        return ListPage();
      // Navigator.pushNamed(context, "/ListPage");
      // break;
      case 2:
        return FavoritePage();
      case 3:
        return ProfilePage();
      default:
        break;
    }
    return GeneralPage();
  }
}

class FavoriteWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    throw UnimplementedError();
  }
}
