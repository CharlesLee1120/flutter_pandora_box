import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class GeneralPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return GeneralPageState();
  }
}

class GeneralPageState extends State<GeneralPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Container(
        height: 400,
        width: double.infinity,
          color: Colors.black12,
          // child: Flexible(
              child:
              Swiper(
        autoplay: true,
        autoplayDelay: 4000,
        // containerWidth: 100,
        // containerHeight: 100,
        // itemWidth: 100,
        // itemHeight: 100,
        itemBuilder: (BuildContext context, int index) {
          return new Image.asset(
              "images/swiper/" + (++index).toString() + ".png");
        },
        onTap: (index) {
          print(index);
        },
        pagination: new SwiperPagination(),
        itemCount: 3,
        viewportFraction: 0.8,
        scale: 0.9,
      )
          // )
      ),
      Container(
        height: 400,
        width: double.infinity,
        color: Colors.blue,
      )
    ]));
  }
}
