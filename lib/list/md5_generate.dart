import 'dart:html';

import 'package:flutter/material.dart';

class MD5GeneratePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MD5GenerateState();
  }
}

class MD5GenerateState extends State<MD5GeneratePage> {
  final _contentController = TextEditingController();

  Widget _buildContentTextField() {
    return TextField(
      controller: this._contentController,
      maxLines: 10,
      textCapitalization: TextCapitalization.sentences,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          labelText: "MD5待加密内容",
          alignLabelWithHint: true,
          hintText: "输入一些文本",
          border: const OutlineInputBorder()),
      onChanged: (val) {
        print(val);
      },
    );
  }

  Widget _buildCommitButton() {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
            minimumSize: const Size(100, 50), maximumSize: const Size(100, 50)),
        onPressed: () {

        },
        child: Text("Generate"));
  }

  Widget _buildClearButton() {
    return TextButton(
        style: ElevatedButton.styleFrom(
            minimumSize: const Size(100, 50), maximumSize: const Size(100, 50)),
        onPressed: () {
          _contentController.clear();
        },
        child: Text("Clear"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MD5 Generate"),
        backgroundColor: Colors.orange,
      ),
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            const SizedBox(
              height: 20.0,
            ),
            _buildContentTextField(),
            const SizedBox(
              height: 20.0,
            ),
            _buildCommitButton(),
            _buildClearButton(),
            const SizedBox(
              height: 20.0,
            )
          ]),
    );
  }
}
