import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

class TimestampConvertPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TimestampConvertState();
  }
}

class TimestampConvertState extends State<TimestampConvertPage> {
  // of the TextField.
  final _timestampTextEditingController = TextEditingController();

  @override
  void initState() {
    _timestampTextEditingController.text =
        new DateTime.now().millisecondsSinceEpoch.toString();
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    _timestampTextEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Timestamp Tools"),
          backgroundColor: Colors.orange,
        ),
        body: SafeArea(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
              child: CupertinoTextField(
                restorationId: 'email_address_text_field',
                keyboardType: TextInputType.emailAddress,
                clearButtonMode: OverlayVisibilityMode.editing,
                autocorrect: false,
                controller: _timestampTextEditingController,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
              child: CupertinoButton(
                // alignment: Alignment.topLeft,
                padding: const EdgeInsets.all(12),
                child: Text('Convert'),
                color: Colors.orange,
                onPressed: () async {
                  // get Input Timestamp value
                  print("Input Value: " + _timestampTextEditingController.text);
                  try {
                    // FormData requestBody = new FormData.fromMap({
                    //   "TimestampValue": _timestampTextEditingController.text
                    // });

                    var requestBody = {
                      'TimestampValue': _timestampTextEditingController.text
                    };
                    print(jsonEncode(requestBody));
                    Response response = await Dio().post(
                        'http://localhost:8080/api/v1/convert/TimestampToDatetime');
                    // Response response = await Dio().get('https://jsonplaceholder.typicode.com/todos/1');
                    print(response.data);
                  } catch (e) {
                    print(e);
                  }
                  // var inputDatetime = new DateTime.fromMillisecondsSinceEpoch(int.parse(_timestampTextEditingController.text), isUtc: true);
                  // print(inputDatetime.year.toString()+","+inputDatetime.month.toString());
                },
              ),
            ),
          ],
        )
            // ListView(
            //   restorationId: 'text_field_demo_list_view',
            //   padding: const EdgeInsets.all(16),
            //   children: [
            //     Padding(
            //       padding: const EdgeInsets.symmetric(vertical: 8),
            //       child: CupertinoTextField(
            //         restorationId: 'email_address_text_field',
            //         keyboardType: TextInputType.emailAddress,
            //         clearButtonMode: OverlayVisibilityMode.editing,
            //         autocorrect: false,
            //       ),
            //     ),
            //     Container(
            //       width: 10,
            //       color: Colors.red,
            //       child: CupertinoButton(
            //         minSize: 22.0,
            //         child: Text('Convert'),
            //       ),
            //     ),
            //     Padding(
            //       padding: const EdgeInsets.symmetric(vertical: 8),
            //       child: CupertinoButton(
            //         minSize: 22.0,
            //         child: Text('Convert'),
            //       ),
            //     ),
            //     Padding(
            //       padding: const EdgeInsets.symmetric(vertical: 8),
            //       child: CupertinoTextField(
            //         restorationId: 'login_password_text_field',
            //         clearButtonMode: OverlayVisibilityMode.editing,
            //         obscureText: true,
            //         autocorrect: false,
            //       ),
            //     ),
            //     CupertinoTextField(
            //       restorationId: 'pin_number_text_field',
            //       prefix: const Icon(
            //         CupertinoIcons.padlock_solid,
            //         size: 28,
            //       ),
            //       padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 12),
            //       clearButtonMode: OverlayVisibilityMode.editing,
            //       keyboardType: TextInputType.number,
            //       decoration: const BoxDecoration(
            //         border: Border(
            //           bottom: BorderSide(
            //             width: 0,
            //             color: CupertinoColors.inactiveGray,
            //           ),
            //         ),
            //       ),
            //     ),
            //   ],
            // ),
            )
        // ListView(
        //   padding: EdgeInsets.symmetric(vertical: 32.0),
        //   children: [Row(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: [
        //       Padding(
        //         padding: const EdgeInsets.symmetric(vertical: 8),
        //         child: CupertinoTextField(
        //           restorationId: 'email_address_text_field',
        //           keyboardType: TextInputType.emailAddress,
        //           clearButtonMode: OverlayVisibilityMode.editing,
        //           autocorrect: false,
        //         ),
        //       ),
        //       Column(
        //         crossAxisAlignment: CrossAxisAlignment.start,
        //         children: [
        //
        //           Text('Enter a Timestamp'),
        //           Container(
        //             width: MediaQuery.of(context).size.width,
        //             height: 32,
        //             color: Colors.white,
        //             child: CupertinoTextField(
        //             ),
        //           ),
        //           Padding(
        //             padding: EdgeInsets.symmetric(vertical: 16.0),
        //           ),
        //           ElevatedButton(
        //             onPressed: () {
        //
        //             },
        //             child: Text('Convert'),
        //           ),
        //           Container(
        //             width: 100,
        //             height: 32,
        //             color: Colors.blue,
        //             child: CupertinoTextField(
        //
        //
        //             ),
        //           ),
        //           Text('Year'),
        //         ],
        //       )
        //     ],
        //   )],
        // )

        // Form(
        //   key: _formKey,
        //   child: Column(
        //     crossAxisAlignment: CrossAxisAlignment.center,
        //     children: <Widget>[
        //       TextFormField(
        //         autofocus: true,
        //         initialValue: new DateTime.now().millisecondsSinceEpoch.toString(),
        //         decoration: const InputDecoration(
        //           hintText: 'Enter Unix Timestamp',
        //         ),
        //         validator: (value) {
        //           if (value.isEmpty) {
        //             return 'Please enter Unix Timestamp';
        //           }
        //           return null;
        //         },
        //       ),
        //       Padding(
        //         padding: const EdgeInsets.symmetric(vertical: 16.0),
        //         child: ElevatedButton(
        //           onPressed: () {
        //             // Validate will return true if the form is valid, or false if
        //             // the form is invalid.
        //             if (_formKey.currentState.validate()) {
        //               // Process data.
        //             }
        //           },
        //           child: Text('Submit'),
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
        );
  }
}
