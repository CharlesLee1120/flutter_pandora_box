import 'package:flutter/material.dart';
import 'package:flutter_pandora_box/list/md5_generate.dart';
import 'package:flutter_pandora_box/list/timestamp_convert.dart';

class ListPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return ListPageState();
  }
}

class ListPageState extends State<ListPage> {

  @override
  Widget build(BuildContext context) {

    List<Widget> list = <Widget>[];

    //Demo数据定义
    var data=[
      {"id":1,"title":"Timestamp Tools","subtitle":"Unix Timestamp convert","page":"TimestampConvertPage"},
      {"id":2,"title":"MD5 Generate","subtitle":"MD5 Generate","page":"MD5GeneratePage"},
    ];

    for (var item in data) {
      var listTileItem = ListTile(
        title: Text(
          item["title"],
          style: TextStyle(
            fontSize: 18.0,
          ),
        ),
        subtitle: Text(item["subtitle"]),
        leading:  Icon(
            Icons.fastfood,
            color:Colors.orange
        ),
        trailing: Icon(Icons.keyboard_arrow_right),
        onTap: () {
          toPage(item["id"]);
        },
      );

      list.add(listTileItem);
    }

    return Container(
      child:Center(
        child: ListView(
          children: list,
        ),
      ),
    );

  }

  void toPage(int _id) {
    switch (_id) {
      case 1:
        Navigator.push(context, new MaterialPageRoute(builder: (context) => new TimestampConvertPage()));
        break;
      case 2:
        Navigator.push(context, new MaterialPageRoute(builder: (context) => new MD5GeneratePage()));
        break;
      case 3:
        break;
      default:
        break;
    }
  }
}
